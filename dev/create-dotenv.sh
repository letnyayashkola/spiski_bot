#!/bin/sh

if [ -f ".env" ]; then
    echo ".env file alredy exists"
    exit 1
fi

cat > .env <<- EOF
SPISKI_BOT_TOKEN=
SPISKI_BOT_USERS_SID=
SPISKI_BOT_RES_SID=
SPISKI_BOT_PROXY=
EOF
