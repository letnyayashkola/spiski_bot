# Info

Бот для ведения списков участников Летней школы.

# Env

| переменная | дефолтное значение | комментарий |
| ---------- | ------------------ | ----------- |
| SPISKI_BOT_TOKEN | - | телеграмовский токен бота |
| SPISKI_BOT_DB_URL | mongodb://localhost | адрес базы |
| SPISKI_BOT_DB_NAME | lshspiski_dev | имя базы |
| SPISKI_BOT_URL | - | адрес для вебхуков* |
| SPISKI_BOT_PORT | 5000 | порт для вебхуков* |
| SPISKI_BOT_USERS_SID | - | айдишник таблицы с пользователями |
| SPISKI_BOT_RES_SID | - | айдишник таблицы с результатами |
| SPISKI_BOT_GOOGLE_TOKEN_FILE | token.pickle | файл с токеном гугла** |
| SPISKI_BOT_PROXY | - | Прокси url*** |

> \* - если значения не заданы, будет использоваться pooling

> ** - получать надо через [google_token_gen](https://gitlab.com/letnyayashkola/google_token_gen)

> *** Только socks5. Может быть полезно, при работе из стран, в которых заблокирован Телеграм.

# Dev

Должен стоять docker, docker-compose и poetry

Установка зависимостей:

```bash
$ poetry install --no-root
```

Инициализация рабочего окружения

```bash
$ make init_env
```

Команда создаст `.env` файл и запустит внешние сервисы-зависимости.
В файл надо вписать нормальные значения.

Запуск бота:

```bash
$ make run
```

Доп команды:

Выключение окружения

```bash
$ make stop_env
```

Запуск окружения по новой

```bash
$ make start_env
```

Удаление окружения и всех данных

```bash
$ make rm_env
```
