import logging
import os
import pickle  # nosec
import re
from collections import namedtuple
from datetime import time

from apiclient.discovery import build

from dotenv import load_dotenv

from google.auth.transport.requests import Request

from telegram.ext import (
    CallbackQueryHandler,
    CommandHandler,
    Filters,
    MessageHandler,
    Updater,
)

from . import __version__
from .handler import Handler

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

LOG = logging.getLogger(__name__)


def load_env():
    try:
        load_dotenv()
    except OSError:
        LOG.info(".env file not found")

    config = namedtuple(
        "Config",
        [
            "token",
            "google_auth_file",
            "users_spreadsheet_id",
            "res_spreadsheet_id",
            "mongo_url",
            "db_name",
            "url",
            "port",
            "proxy",
        ],
    )

    config.token = os.getenv("SPISKI_BOT_TOKEN")
    config.google_auth_file = os.getenv(
        "SPISKI_BOT_GOOGLE_TOKEN_FILE", os.path.join(os.getcwd(), "token.pickle")
    )
    # Таблица с пользователями должна иметь следующий вид
    # username, мастерская, режим бога (может отсутствовать)
    # первая строка - заголовки
    config.users_spreadsheet_id = os.getenv("SPISKI_BOT_USERS_SID")
    config.res_spreadsheet_id = os.getenv("SPISKI_BOT_RES_SID")
    config.mongo_url = os.getenv("SPISKI_BOT_DB_URL", "mongodb://localhost")
    config.db_name = os.getenv("SPISKI_BOT_DB_NAME", "lshspiski_dev")
    config.url = os.getenv("SPISKI_BOT_URL")
    config.port = int(os.getenv("SPISKI_BOT_PORT", "5000"))
    config.proxy = os.getenv("SPISKI_BOT_PROXY")

    return config


def setup_google(auth_file):
    with open(auth_file, "rb") as token:
        creds = pickle.load(token)  # nosec

    if creds.expired and creds.refresh_token:
        creds.refresh(Request())
        with open(auth_file, "wb") as token:
            pickle.dump(creds, token)

    return build("sheets", "v4", credentials=creds)


def request_kwargs(proxy):
    kwargs: dict = {"read_timeout": 10, "connect_timeout": 10}

    if proxy is not None:
        kwargs["proxy_url"] = proxy

    return kwargs


def main():
    LOG.info("Start spiski bot, version: %s", __version__)

    config = load_env()

    updater = Updater(
        token=config.token,
        request_kwargs=request_kwargs(config.proxy),
        use_context=True,
    )

    service = setup_google(config.google_auth_file)

    handler = Handler(
        config.url,
        config.db_name,
        config.res_spreadsheet_id,
        config.users_spreadsheet_id,
        service,
    )

    dispatcher = updater.dispatcher

    dispatcher.add_handler(CallbackQueryHandler(handler.inline_button_event))
    dispatcher.add_handler(CommandHandler("start", handler.start))
    dispatcher.add_handler(CommandHandler("help", handler.help_))
    dispatcher.add_handler(CommandHandler("whoisyourdaddy", handler.whoisyourdaddy))
    dispatcher.add_handler(CommandHandler("get", handler.get_people))
    dispatcher.add_handler(CommandHandler("status", handler.status))
    dispatcher.add_handler(CommandHandler("del", handler.del_people, pass_args=True))
    dispatcher.add_handler(CommandHandler("sync", handler.force_sync))
    dispatcher.add_handler(
        MessageHandler(
            Filters.regex(re.compile("Отвали", re.IGNORECASE)), handler.cancel_notify
        )
    )
    dispatcher.add_handler(MessageHandler(Filters.text, handler.handle_text_message))
    dispatcher.add_handler(MessageHandler(Filters.all, handler.handle_other_message))
    dispatcher.add_error_handler(handler.error)

    updater.job_queue.run_repeating(handler.get_users, interval=3600, first=0)

    updater.job_queue.run_repeating(handler.update_sheet, interval=3600, first=60)

    updater.job_queue.run_daily(handler.send_status_daily, time(23, 59))

    updater.job_queue.run_daily(handler.send_notifications, time(21))
    updater.job_queue.run_daily(handler.send_notifications, time(22))
    updater.job_queue.run_daily(handler.send_notifications, time(23))

    updater.job_queue.run_daily(handler.send_notifications, time(23, 30))

    updater.job_queue.run_daily(handler.send_notifications, time(23, 45))

    updater.job_queue.run_daily(
        handler.send_notifications,
        time(0, 0),
        context={"text": "Вы забыли про списки", "need_cleanup": True},
    )

    if config.url:
        updater.start_webhook(
            listen="0.0.0.0", port=config.port, url_path=config.token  # nosec
        )
        url = config.url
        if url.endswith("/"):
            url = url[:-1]
        updater.bot.set_webhook(url=f"{url}/{config.token}")

        LOG.info("URL: %s", f"{url}/{config.token}")
        LOG.info("Port: %s", config.port)
    else:
        updater.start_polling()

    updater.idle()
