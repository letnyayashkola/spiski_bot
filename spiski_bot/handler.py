import logging
import re
from datetime import datetime, time

from googleapiclient.errors import HttpError

from pymongo import MongoClient

import pymorphy2

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import Unauthorized
from telegram.ext import CallbackContext, run_async
from telegram.update import Update as StateUpdate

from . import __version__
from .config import GOD_MESSAGE, MESSAGE

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

LOG = logging.getLogger(__name__)


class Handler:
    def __init__(self, url, db, res_id, users_id, google_service):
        self._users = {}
        self._mongo = MongoClient(url)
        self._db = db
        self._morph_anal = pymorphy2.MorphAnalyzer()
        self._res_id = res_id
        self._users_id = users_id
        self._g_service = google_service

    def error(self, update: StateUpdate, context: CallbackContext):
        data: dict = {}
        if update and update.message:
            data = update.message.to_dict()

        LOG.error("Error while processing update: %s, %s", data, context.error)

    def whoisyourdaddy(self, update: StateUpdate, context: CallbackContext):
        update.message.reply_text(
            f"IT-изба, 2019-2020\nВерсия: {__version__}\n"
            + "https://it.letnyayashkola.org"
        )

    def help_(self, update: StateUpdate, context: CallbackContext):
        try:
            is_god = self._users[update.message.from_user.username]["god_mode"]
        except KeyError:
            update.message.reply_text("У тебя нет прав. Иди в избу")
            return

        if is_god:
            update.message.reply_text(GOD_MESSAGE)
        else:
            update.message.reply_text(MESSAGE)

    def start(self, update: StateUpdate, context: CallbackContext):
        collection = self._mongo[self._db]["users"]

        username = update.message.from_user.username
        if username is None:
            update.message.reply_text(
                "У тебя должен быть задан @username. "
                + "Создай его и выполни заново команду /start"
            )
            return

        try:
            is_god = self._users[update.message.from_user.username]["god_mode"]
        except KeyError:
            update.message.reply_text("У тебя нет прав. Иди в избу")
            return

        collection.update_one(
            {"data": True}, {"$set": {username: update.message.chat_id}}, upsert=True
        )

        if is_god:
            update.message.reply_text(GOD_MESSAGE)
        else:
            update.message.reply_text(MESSAGE)

    def status(self, update: StateUpdate, context: CallbackContext):
        try:
            is_god = self._users[update.message.from_user.username]["god_mode"]
        except KeyError:
            self.handle_other_message(update, context)
            return

        if not is_god:
            self.handle_other_message(update, context)
            return

        result = self.build_status()
        update.message.reply_text(result)

    def build_status(self):
        data_collection = self._mongo[self._db]["data"]
        users_collection = self._mongo[self._db]["users"]

        result = ""

        last_update = users_collection.find_one({"last_update": True}) or {}
        all_workshops = set(map(lambda x: x["workshop"], self._users.values()))

        for item in all_workshops:
            result += item + ": " + ("+" if item in last_update else "-") + "\n"

        result += "\n\n"
        underage = 0
        adult = 0

        for category in ["школьник", "участник", "преподаватель", "координатор"]:
            people_count: int = data_collection.count({"status": category})
            if category == "школьник":
                underage = people_count
            else:
                adult += people_count

            result += category + ": " + str(people_count) + "\n"

        result += "\nвсего людей: " + str(underage + adult) + "\n"
        result += "всего совершеннолетних: " + str(adult) + "\n"
        result += "всего несовершеннолетних: " + str(underage)

        return result

    def send_status_daily(self, context: CallbackContext):
        collection = self._mongo[self._db]["users"]
        chat_ids = collection.find_one({"data": True})

        result = self.build_status()

        gods = [k for k, v in self._users.items() if v["god_mode"]]

        for username in gods:
            try:
                context.bot.send_message(chat_id=chat_ids[username], text=result)
            except (Unauthorized, KeyError):
                continue

    def del_people(self, update: StateUpdate, context: CallbackContext):
        args_workshop = " ".join(context.args)
        try:
            workshop = self._users[update.message.from_user.username]["workshop"]
            is_god = self._users[update.message.from_user.username]["god_mode"]
        except KeyError:
            update.message.reply_text("У тебя нет прав. Иди в избу")
            return

        if args_workshop and is_god:
            workshop = args_workshop

        keyboard = [
            [
                InlineKeyboardButton("Да", callback_data="1_" + workshop),
                InlineKeyboardButton("Нет", callback_data="2_" + workshop),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)

        update.message.reply_text(
            (
                "Ты собираешься удалить всех участников мастерской {0}." + " Вы верены?"
            ).format(workshop),
            reply_markup=reply_markup,
        )

    def cancel_notify(self, update: StateUpdate, context: CallbackContext):
        now = datetime.now().time()
        begin = time(21)
        end = time(23, 59, 59)

        if not (begin < now and now < end):
            update.message.reply_text("Я и не пристаю.")
            return

        collection = self._mongo[self._db]["users"]

        last_update = collection.find_one({"last_update": True}) or {}

        if self._users[update.message.from_user.username]["workshop"] in last_update:
            update.message.reply_text("Я и не пристаю.")
            return

        already_blacklisted = collection.find_one(
            {"blacklist": True, update.message.from_user.username: True}
        )
        if already_blacklisted is not None:
            update.message.reply_text("Сам отвали")
            return

        keyboard = [
            [
                InlineKeyboardButton(
                    "Да", callback_data="3_" + update.message.from_user.username
                ),
                InlineKeyboardButton("Нет", callback_data="4"),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)

        update.message.reply_text(
            "Ты уверен, что состав мастерской не изменился?", reply_markup=reply_markup
        )

    def inline_button_event(self, update: StateUpdate, context: CallbackContext):
        query = update.callback_query
        action = query.data[0]

        if action == "1":
            workshop = query.data[2:]

            collection = self._mongo[self._db]["data"]
            collection.remove({"workshop": re.compile(workshop, re.IGNORECASE)})
            text = "Я сделаль"
        elif action == "2":
            text = "Окей. Я не буду это делать"
        elif action == "3":
            usename = query.data[2:]

            collection = self._mongo[self._db]["users"]
            collection.update_one(
                {"blacklist": True}, {"$set": {usename: True}}, upsert=True
            )

            text = "Ок. Я не буду беспокоить тебя сегодня"
        elif action == "4":
            text = "Вот поэтому я и напоминаю тебе об этом"

        context.bot.edit_message_text(
            text=text,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )

        query.answer(text="")

    def get_people(self, update: StateUpdate, context: CallbackContext):
        try:
            workshop = self._users[update.message.from_user.username]["workshop"]
        except KeyError:
            update.message.reply_text("У тебя нет прав. Иди в избу")
            return

        collection = self._mongo[self._db]["data"]

        counters = {
            "школьник": 0,
            "участник": 0,
            "преподаватель": 0,
            "координатор": 0,
            "всего": 0,
        }

        message = ""
        for item in collection.find({"workshop": workshop}).sort("last_name"):
            message += "<b>" + item["last_name"] + " " + item["first_name"]
            if item["middle_name"]:
                message += " " + item["middle_name"]
            message += "</b>, " + item["status"] + "\n"
            counters[item["status"]] += 1
            counters["всего"] += 1

        if message == "":
            update.message.reply_text("В мастерской {0} нет людей".format(workshop))
            return

        message = "Участники мастерской {0}:\n\n".format(workshop) + message + "\n"

        for key, value in counters.items():
            message += "<i>{0}</i>: {1}\n".format(key, value)

        update.message.reply_html(message)

    @staticmethod
    def user_from_text_row(item, workshop, del_mode):
        item = item.split(",")

        user = {}
        user["workshop"] = workshop

        fio = item[0].split(" ")
        fio = list(filter(None, fio))

        last_name = fio[0].strip()
        first_name = fio[1].strip()

        try:
            middle_name = " ".join(fio[2:]).strip()
        except IndexError:
            middle_name = ""

        user["last_name"] = last_name
        user["first_name"] = first_name
        user["middle_name"] = middle_name
        if not del_mode:
            user["status"] = item[1].strip()

        return user

    def handle_other_message(self, update: StateUpdate, context: CallbackContext):
        update.message.reply_text("Очень смешно...")

    def force_sync(self, update: StateUpdate, context: CallbackContext):
        try:
            is_god = self._users[update.message.from_user.username]["god_mode"]
        except KeyError:
            self.handle_other_message(update, context)
            return

        if not is_god:
            self.handle_other_message(update, context)
            return

        self._update_sheet()

        update.message.reply_text("Успешно синхронизировано")

    @run_async
    def handle_text_message(self, update: StateUpdate, context: CallbackContext):
        try:
            workshop = self._users[update.message.from_user.username]["workshop"]
        except KeyError:
            update.message.reply_text("У тебя нет прав. Иди в избу")
            return

        collection = self._mongo[self._db]["data"]

        self._users[update.message.from_user.username][
            "chat_id"
        ] = update.message.chat_id

        data = update.message.text.split("\n")

        if data[0].strip().lower() == "+":
            del_mode = False
        elif data[0].strip().lower() == "-":
            del_mode = True
        else:
            update.message.reply_text("Я не понимать. Плохой формат :( См. /help")
            return

        if len(data) == 1 or (len(data) == 2 and (data[1] == "-" or data[1] == "+")):
            update.message.reply_text("Этого недостаточно. См. /help")
            return

        for item in data[1:]:
            if item.strip() == "":
                update.message.reply_text(
                    "Пустая строка - это нарушение формата. См. /help"
                )
                return

            if not del_mode and item.strip().lower() == "-":
                del_mode = True
                continue
            elif del_mode and item.strip().lower() == "+":
                del_mode = False
                continue

            try:
                user = self.user_from_text_row(item, workshop, del_mode)
            except IndexError:
                update.message.reply_text(
                    "Не могу распарсить строку {0}. ".format(item)
                    + "Скорее всего, кривой формат. См. /help"
                )
                return

            if ("Surn" in self._morph_anal.parse(user["first_name"])[0].tag) and (
                "Name" in self._morph_anal.parse(user["last_name"])[0].tag
            ):
                update.message.reply_text(
                    (
                        "Есть подозрение, что вы перепутали имя и фамилию "
                        + "участника {0} местами. См. /help"
                    ).format(item.split(",")[0])
                )
                return

            if (not del_mode) and (
                user["status"]
                not in ("школьник", "участник", "преподаватель", "координатор")
            ):
                update.message.reply_text(
                    "Плохой статус для участника {0}.\n".format(item.split(",")[0])
                    + "Допустимы: школьник, участник, "
                    + "преподаватель, координатор. См. /help"
                )
                return

            if not del_mode:
                if (
                    collection.count(
                        {
                            "workshop": user["workshop"],
                            "last_name": re.compile(user["last_name"], re.IGNORECASE),
                            "first_name": re.compile(user["first_name"], re.IGNORECASE),
                            "middle_name": re.compile(
                                user["middle_name"], re.IGNORECASE
                            ),
                        }
                    )
                    == 0
                ):
                    user["date"] = datetime.utcnow()
                    collection.insert(user)
            else:
                collection.remove(user)

        self._mongo[self._db]["users"].update_one(
            {"last_update": True}, {"$set": {workshop: True}}, upsert=True
        )
        update.message.reply_text("Принято! Спасибо")

    def send_notifications(self, context: CallbackContext):
        text = context.job.context.get("text", "Не забудь про списки")

        collection = self._mongo[self._db]["users"]

        chat_ids = collection.find_one({"data": True})
        last_update = collection.find_one({"last_update": True}) or {}
        blacklist = collection.find_one({"blacklist": True}) or {}

        for username, value in self._users.items():
            if (value["workshop"] not in last_update) and (username not in blacklist):
                try:
                    context.bot.send_message(chat_id=chat_ids[username], text=text)
                except (Unauthorized, KeyError):
                    continue

        if (
            "need_cleanup" in context.job.context
            and context.job.context["need_cleanup"]
        ):
            collection.delete_one({"last_update": True})
            collection.delete_one({"blacklist": True})

    def update_sheet(self, context: CallbackContext):
        self._update_sheet()

    def _update_sheet(self):
        collection = self._mongo[self._db]["data"]

        current_date = datetime.now()

        range_name = "{0:02d}.{1:02d}!A:E".format(current_date.day, current_date.month)

        create_body = {
            "requests": [
                {
                    "addSheet": {
                        "properties": {
                            "title": "{0:02d}.{1:02d}".format(
                                current_date.day, current_date.month
                            )
                        }
                    }
                }
            ]
        }

        try:
            self._g_service.spreadsheets().batchUpdate(
                spreadsheetId=self._res_id, body=create_body
            ).execute()
        except HttpError:
            LOG.info("Sheet already created")

        values = [
            [
                "Мастерская",
                "Категория участника (школьник, участник, "
                + "преподаватель, координатор)",
                "Фамилия",
                "Имя",
                "Отчество",
            ]
        ]

        for item in collection.find().sort("workshop"):
            values.append(
                [
                    item["workshop"],
                    item["status"],
                    item["last_name"],
                    item["first_name"],
                    item["middle_name"],
                ]
            )

        self._g_service.spreadsheets().values().clear(
            spreadsheetId=self._res_id, range=range_name, body={}
        ).execute()

        self._g_service.spreadsheets().values().update(
            spreadsheetId=self._res_id,
            valueInputOption="RAW",
            range=range_name,
            body={"values": values},
        ).execute()

        cat_count = []

        for cat in ["школьник", "участник", "преподаватель", "координатор"]:
            cat_count.append([cat, collection.count({"status": cat})])

        cat_count.append(["всего", collection.count()])

        range_name = "{0:02d}.{1:02d}!G:H".format(current_date.day, current_date.month)

        self._g_service.spreadsheets().values().update(
            spreadsheetId=self._res_id,
            valueInputOption="RAW",
            range=range_name,
            body={"values": cat_count},
        ).execute()

    def get_users(self, context: CallbackContext):
        # Call the Sheets API
        RANGE_NAME = "A:C"
        result = (
            self._g_service.spreadsheets()
            .values()
            .get(spreadsheetId=self._users_id, range=RANGE_NAME)
            .execute()
        )
        values = result.get("values", [])

        data = {}
        for item in values[1:]:
            item[0] = item[0].strip()
            if item[0].startswith("@"):
                item[0] = item[0][1:]
            data[item[0]] = {
                "workshop": item[1].strip(),
                "god_mode": len(item) == 3 and item[2].strip() != "",
            }

        LOG.info(data)

        self._users = data
