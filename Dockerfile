FROM inn0kenty/pyinstaller-alpine:3.6 as builder

WORKDIR /build

RUN apk update && apk --no-cache add tzdata \
    && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime \
    && echo "Europe/Moscow" > /etc/timezone \
    && apk del tzdata \
    && pip install poetry

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config virtualenvs.create false \
    && poetry install --no-root --no-dev --no-interaction

COPY . .

RUN pyinstaller --noconfirm --clean --onefile --name app \
    --hidden-import=apiclient \
    --additional-hooks-dir=hooks \
    run.py

FROM alpine:3.11

RUN addgroup -g 1000 -S app \
    && adduser -S -u 1000 -G app app

LABEL maintainer="Innokenty Lebedev <i.lebedev@letnyayashkola.org>"

COPY --from=builder --chown=app:app /build/dist/app .
COPY --from=builder /etc/localtime /etc/localtime
COPY --from=builder /etc/timezone /etc/timezone

USER app

ENTRYPOINT ["./app"]
