NAME=spiski_bot

init_env:
	./dev/create-dotenv.sh
	docker-compose -p ${NAME} -f dev/docker-compose.yml up -d

start_env:
	docker-compose -p ${NAME} -f dev/docker-compose.yml start

stop_env:
	docker-compose -p ${NAME} -f dev/docker-compose.yml stop

rm_env:
	docker-compose -p ${NAME} -f dev/docker-compose.yml rm

run:
	poetry run python run.py
